/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.services.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import application.models.response.user.User;

public class UserMapper implements ResultSetMapper<User> {
	@Override
	public User map(int index, ResultSet r, StatementContext ctx) throws SQLException {
		User user = new User();
		user.setId(r.getInt(User.ID));
		user.setUser_id(r.getString(User.USER_ID));
		user.setUsername(r.getString(User.USERNAME));
		user.setPassword(r.getString(User.PASSWORD));
		user.setSalt(r.getString(User.SALT));
		user.setHash_algorithm(r.getString(User.HASH_ALGORITHM));
		user.setGiven_name(r.getString(User.GIVERN_NAME));
		user.setSurname(r.getString(User.SURNAME));

		return user;
	}
}
