/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.services.accessors;

import java.util.UUID;

import application.models.request.user.UserPostRequest;
import application.models.request.user.UserUpdateRequest;
import application.models.response.user.User;
import application.services.QualityAssuranceDBIAccessor;
import application.services.daos.UserDao;
import application.services.exceptions.DBICreationException;

public interface UserAccessor {

	public static User getById(Integer id) throws DBICreationException {
		UserDao dao = QualityAssuranceDBIAccessor.build().onDemand(UserDao.class);
		User object = dao.getUserById(id);
		dao.close();

		return object;
	}

	public static User getByUuid(String uuid) throws DBICreationException {
		UserDao dao = QualityAssuranceDBIAccessor.build().onDemand(UserDao.class);
		User object = dao.getUserByUuid(uuid);
		dao.close();

		return object;
	}

	public static User getByUsername(String username) throws DBICreationException {
		UserDao dao = QualityAssuranceDBIAccessor.build().onDemand(UserDao.class);
		User object = dao.getUserByUsername(username);
		dao.close();

		return object;
	}

	public static User post(UserPostRequest user) throws DBICreationException {
		UserDao dao = QualityAssuranceDBIAccessor.build().onDemand(UserDao.class);

		// TODO Validate Input
		String user_id = UUID.randomUUID().toString();
		String salt = ""; // TODO Generate SALT
		String hash = ""; // TODO Generate HASH

		Integer inserted_id = dao.post(user_id, user.getUsername(), user.getPassword(), salt, hash,
				user.getGiven_name(), user.getSurname());
		User response = getById(inserted_id);
		dao.close();

		return response;
	}

	public static User updateUser(String uuid, UserUpdateRequest request) throws DBICreationException {
		UserDao dao = QualityAssuranceDBIAccessor.build().onDemand(UserDao.class);

		User user = getByUuid(uuid);

		// TODO Validate Input
		if (request.getUsername() != null) {
			user.setUsername(request.getUsername());
		}
		if (request.getPassword() != null) {
			user.setPassword(request.getPassword());
			// TODO Generate new Salt & Hash
		}
		if (request.getGiven_name() != null) {
			user.setGiven_name(request.getGiven_name());
		}
		if (request.getSurname() != null) {
			user.setSurname(request.getSurname());
		}

		dao.update(user.getId(), user.getUser_id(), user.getUsername(), user.getPassword(), user.getSalt(),
				user.getHash_algorithm(), user.getGiven_name(), user.getSurname());
		User response = getByUuid(uuid);
		dao.close();

		return response;
	}

	public static User delete(String uuid) throws DBICreationException {
		UserDao dao = QualityAssuranceDBIAccessor.build().onDemand(UserDao.class);

		User response = getByUuid(uuid);
		dao.deleteUser(response.getUser_id());

		dao.close();
		return response;
	}
}
