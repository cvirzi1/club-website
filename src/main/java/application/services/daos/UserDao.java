/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.services.daos;

import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

import application.models.response.user.User;
import application.services.mappers.UserMapper;

public interface UserDao {
	@SqlQuery()
	@Mapper(UserMapper.class)
	public User getUserById(Integer id);

	@SqlQuery()
	@Mapper(UserMapper.class)
	public User getUserByUuid(String uuid);

	@SqlQuery()
	@Mapper(UserMapper.class)
	public User getUserByUsername(String username);

	@SqlUpdate()
	@GetGeneratedKeys
	public Integer post(String user_id, String username, String password, String salt, String hash_algorithm,
			String given_name, String surname);

	@SqlUpdate()
	public void update(int id, String user_id, String username, String password, String salt, String hash_algorithm,
			String given_name, String surname);

	@SqlUpdate()
	public void deleteUser(String uuid);

	void close();
}
